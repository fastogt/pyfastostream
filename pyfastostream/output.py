from abc import ABC
from sys import stdout
from typing import BinaryIO, Optional
from pathlib import Path


class Output(ABC):
    def __init__(self) -> None:
        self.opened = False

    def open(self) -> None:
        self._open()
        self.opened = True

    def close(self) -> None:
        if self.opened:
            self._close()

        self.opened = False

    def write(self, data):
        if not self.opened:
            raise OSError("Output is not opened")

        return self._write(data)

    def _open(self):
        pass

    def _close(self):
        pass

    def _write(self, data):
        pass


class FileOutput(Output):
    def __init__(
        self,
        filename: Optional[Path] = None,
        fd: Optional[BinaryIO] = None,
        record: Optional["FileOutput"] = None,
    ) -> None:
        super().__init__()
        self.filename = filename
        self.fd = fd
        self.record = record

    def _open(self):
        if self.filename:
            self.filename.parent.mkdir(parents=True, exist_ok=True)
            self.fd = open(self.filename, "wb")

        if self.record:
            self.record.open()

    def _close(self):
        if self.fd and self.fd is not stdout:
            self.fd.close()
        if self.record:
            self.record.close()

    def _write(self, data):
        if self.fd:
            self.fd.write(data)
        if self.record:
            self.record.write(data)
