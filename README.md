About Pyfastostream
===============

Pyfastotstream python files.

Dependencies
========
`setuptools` `requests`

Install
========
`python3 setup.py install` or `pip3 install .`

Usage
========
`pyfastostream --url https://youtu.be/1234567890`
